#!/usr/bin/env python3

from MAGS import *

def main():
    df = get_key(5111)

    idx = pd.MultiIndex.from_product([ ks_names.keys(), df.index ], names=['reg', 'date'])
    casesMAGS = pd.DataFrame(index = idx, columns=['cases','deaths','recov','popul']) .astype(np.float)

    # download all IDs, place in DataFrame
    for k in ks_names.keys():
    #for k in [5570]:
        try:
            df = get_key(k)
            casesMAGS.loc[pd.IndexSlice[k,'cases']] = df['anzahlMKumuliert'].values
            casesMAGS.loc[pd.IndexSlice[k,'deaths']] = df['verstorbenKumuliert'].values
            casesMAGS.loc[pd.IndexSlice[k,'recov']] = df['genesenKumuliert'].values
            casesMAGS.loc[pd.IndexSlice[k,'popul']] = ks_popul[k]
            casesMAGS.loc[pd.IndexSlice[k,'delayed']] = ser_delayed( casesMAGS.loc[k]['cases'] , COVIDDELAY_d ).values
            casesMAGS.loc[pd.IndexSlice[k,'7d']] = ser_delayed( casesMAGS.loc[k]['cases'] , 7 ).values

            print(k, "Ok")
        except Exception as e:
            print(k, "Error: ", e)
    casesMAGS.sort_index(inplace=True)
    # backup
    casesMAGS.to_csv("MAGS.csv") 

if __name__ == '__main__':
    main()
