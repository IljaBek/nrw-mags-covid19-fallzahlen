#!/usr/bin/env python3

from MAGS import *


def main():
    casesMAGS = pd.read_csv('MAGS.csv', sep=',', dayfirst=True, parse_dates=True, index_col=[0,1])


    ### Plot Production Loop
    log = open("README.md", 'w')
    log.write("""# SARS-COV-2 Case numbers in NRW, D
* https://www.mags.nrw/coronavirus-fallzahlen-nrw


""")

    for P in [('cases', "Cumulative Cases" ), ('delayed', "Cumulative  cases - cases-{:}d-earlier".format(COVIDDELAY_d) )]:
        fn,_,_ = plot_cum(casesMAGS, *P)
        log.write("""## {:}
![plot of {:}](./{:})

""".format(P[1], P[0], fn))
        print(P[0], 'done!')


    for k in ks:
#    for k in [5570]:
        fn,f,_,_ = plot_key_2p(casesMAGS, k)
        plt.close(f)
        log.write("""## {:}
![plot for {:}](./{:})

""".format(ks_names[k], k, fn))
        print(k, 'done!')
    
    log.close()

if __name__ == '__main__':
    main()
