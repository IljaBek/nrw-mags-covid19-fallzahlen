# SARS-COV-2 Case numbers in NRW, D
* https://www.mags.nrw/coronavirus-fallzahlen-nrw


## Cumulative Cases
![plot of cases](./plot_cum_cases.png)

## Cumulative  cases - cases-14d-earlier
![plot of delayed](./plot_cum_delayed.png)

## Düsseldorf
![plot for 5111](./plot_5111.png)

## Duisburg
![plot for 5112](./plot_5112.png)

## Essen
![plot for 5113](./plot_5113.png)

## Krefeld
![plot for 5114](./plot_5114.png)

## Mönchengladbach
![plot for 5116](./plot_5116.png)

## Mülheim an der Ruhr
![plot for 5117](./plot_5117.png)

## Oberhausen
![plot for 5119](./plot_5119.png)

## Remscheid
![plot for 5120](./plot_5120.png)

## Solingen
![plot for 5122](./plot_5122.png)

## Wuppertal
![plot for 5124](./plot_5124.png)

## Kleve
![plot for 5154](./plot_5154.png)

## Mettmann
![plot for 5158](./plot_5158.png)

## Rhein-Kreis Neuss
![plot for 5162](./plot_5162.png)

## Viersen
![plot for 5166](./plot_5166.png)

## Wesel
![plot for 5170](./plot_5170.png)

## Bonn
![plot for 5314](./plot_5314.png)

## Köln
![plot for 5315](./plot_5315.png)

## Leverkusen
![plot for 5316](./plot_5316.png)

## Städteregion Aachen
![plot for 5334](./plot_5334.png)

## Düren
![plot for 5358](./plot_5358.png)

## Rhein-Erft-Kreis
![plot for 5362](./plot_5362.png)

## Euskirchen
![plot for 5366](./plot_5366.png)

## Heinsberg
![plot for 5370](./plot_5370.png)

## Oberbergischer Kreis
![plot for 5374](./plot_5374.png)

## Rheinisch-Bergischer Kreis
![plot for 5378](./plot_5378.png)

## Rhein-Sieg-Kreis
![plot for 5382](./plot_5382.png)

## Bottrop
![plot for 5512](./plot_5512.png)

## Gelsenkirchen
![plot for 5513](./plot_5513.png)

## Münster
![plot for 5515](./plot_5515.png)

## Borken
![plot for 5554](./plot_5554.png)

## Coesfeld
![plot for 5558](./plot_5558.png)

## Recklinghausen
![plot for 5562](./plot_5562.png)

## Steinfurt
![plot for 5566](./plot_5566.png)

## Warendorf
![plot for 5570](./plot_5570.png)

## Bielefeld
![plot for 5711](./plot_5711.png)

## Gütersloh
![plot for 5754](./plot_5754.png)

## Herford
![plot for 5758](./plot_5758.png)

## Höxter
![plot for 5762](./plot_5762.png)

## Lippe
![plot for 5766](./plot_5766.png)

## Minden-Lübbecke
![plot for 5770](./plot_5770.png)

## Paderborn
![plot for 5774](./plot_5774.png)

## Bochum
![plot for 5911](./plot_5911.png)

## Dortmund
![plot for 5913](./plot_5913.png)

## Hagen
![plot for 5914](./plot_5914.png)

## Hamm
![plot for 5915](./plot_5915.png)

## Herne
![plot for 5916](./plot_5916.png)

## Ennepe-Ruhr-Kreis
![plot for 5954](./plot_5954.png)

## Hochsauerlandkreis
![plot for 5958](./plot_5958.png)

## Märkischer Kreis
![plot for 5962](./plot_5962.png)

## Olpe
![plot for 5966](./plot_5966.png)

## Siegen-Wittgenstein
![plot for 5970](./plot_5970.png)

## Soest
![plot for 5974](./plot_5974.png)

## Unna
![plot for 5978](./plot_5978.png)

