#!/usr/bin/env python3

from MAGS import *


def main(KEY):
    casesMAGS = pd.read_csv('MAGS.csv', sep=',', dayfirst=True, parse_dates=True, index_col=[0,1])

    for k in [KEY]:
        try:
#            df = get_key(k)
            df = reread_key(k)
            casesMAGS.loc[pd.IndexSlice[k,'cases']] = df['anzahlEKumuliert'].values
            casesMAGS.loc[pd.IndexSlice[k,'deaths']] = df['verstorbenEKumuliert'].values
            casesMAGS.loc[pd.IndexSlice[k,'recov']] = df['genesenKumuliert'].values
            casesMAGS.loc[pd.IndexSlice[k,'popul']] = ks_popul[k]
            casesMAGS.loc[pd.IndexSlice[k,'delayed']] = ser_delayed( casesMAGS.loc[k]['cases'] , COVIDDELAY_d ).values

            print(k, "Ok")
        except Exception as e:
            print(k, "Error: ", e)


#    for P in [('cases', "Cumulative Cases" ), ('delayed', "Cumulative (calc) delta delay {:}d".format(COVIDDELAY_d) )]:
#        fn,_,_ = plot_cum(casesMAGS, *P)


    for k in [KEY]:
        fn,f,_,_ = plot_key(casesMAGS, k)
        plt.close(f)

import sys

if __name__ == '__main__':
    main(int(sys.argv[1]))
