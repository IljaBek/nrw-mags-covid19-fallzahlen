#!/usr/bin/env python3

import numpy as np
import pandas as pd

import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
plt.style.use('classic')
from matplotlib.ticker import (AutoMinorLocator, MultipleLocator)

from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()


### Read region IDs, names and populations
# Source: https://www.destatis.de/DE/Themen/Laender-Regionen/Regionales/Gemeindeverzeichnis/Administrativ/04-kreise.xlsx


kreis = pd.read_csv('kreise_20191231.csv')
kreis['ID'] = kreis['name'].astype("str").str.split(',').str[0]

cut = (kreis['key']>5000)&(kreis['key']<6000)
ks_names = pd.Series(kreis[cut]['ID'].values,index=kreis[cut]['key']).to_dict() 
ks_popul = pd.Series(kreis[cut]['population'].values,index=kreis[cut]['key']).to_dict() 
ks_names[5111]
ks = kreis[cut]['key'].values
ks

### Assemble NRW MAGS data

#casesMAGS = None

import requests
import string
import io

# ID = 5334
# dowload file
def get_key(ID):
  iURL = "https://www.lzg.nrw.de/covid19/daten/covid19_zeitreihe_{:}.csv"
  iURL = iURL.format(ID) 
  #os.system('curl '+iurl + ' > mags.i')
  cont = requests.get(iURL).text
  printable = set(string.printable)
  cont = ''.join(filter(lambda x: x in printable, cont))
  df = pd.read_csv(io.StringIO(cont), sep=',', dayfirst=True, parse_dates=True, index_col=[1])
  df = df[df['kreis']!=5]
  df.to_csv('covid19_zeitreihe_{:}.csv'.format(ID))
  return df

def reread_key(ID):
    return  pd.read_csv('covid19_zeitreihe_{:}.csv'.format(ID), sep=',', dayfirst=True, parse_dates=True, index_col=[0])

### Data Preparation

COVIDDELAY_d = 14 # guess from data

def ser_delayed(ser, COVIDDELAY_d = 15):
  ser_del = ser.copy()
  ser_del.index = ser.index + pd.DateOffset(days=COVIDDELAY_d)
  
  set_act_del =  ser.resample('D').mean().interpolate(method='linear') - ser_del.resample('D').mean().interpolate(method='linear')
#   df_tmp = pd.concat( [pd.DataFrame( { 'x': ser}), pd.DataFrame( { 'y': set_act_del })], axis=1)
  df_tmp = pd.DataFrame( { 'x': ser}).join( pd.DataFrame( { 'y': set_act_del }))
  return df_tmp['y']


def plot_key(casesMAGS, k):
  fig, ax = plt.subplots(nrows=1, ncols=1, sharex=False, sharey=False, num=None, figsize=(1000/80.,400/80. ), dpi=80, facecolor='w', edgecolor='k')
  cl = 'tab:blue'
  cl2 = 'black'
  ax.set_title(ks_names[k])
  ax.set_ylabel("... Cases per 100.000", color=cl)
  ax2 = ax.twinx()
  ax.tick_params(axis='y', labelcolor=cl)
  ax2.tick_params(axis='y', labelcolor=cl2)
  ax2.set_ylabel("Active Cases per 100.000", color=cl2)
  ax2.plot(( casesMAGS.loc[k]['delayed']/casesMAGS.loc[k]['popul']*100000 ), lw=0.8, ms=1, marker='o', label=' cases - cases-{:}d-earlier'.format(COVIDDELAY_d))  
  ax.plot(( casesMAGS.loc[k]['cases']/casesMAGS.loc[k]['popul']*100000 ),lw=0.3, label='Total Diagnosed')
  ax.plot(( casesMAGS.loc[k]['deaths']*100/casesMAGS.loc[k]['popul']*100000 ),lw=0.3, label='Deaths*100')
  ax.plot(( (casesMAGS.loc[k]['deaths']+casesMAGS.loc[k]['recov'])/casesMAGS.loc[k]['popul']*100000 ),lw=0.3, label='Recoveries+Deaths')
  ax2.plot(( (casesMAGS.loc[k]['cases']-casesMAGS.loc[k]['deaths']-casesMAGS.loc[k]['recov'])/casesMAGS.loc[k]['popul']*100000 ), label='Active')
  ax2.axhline(y=50, color='red', linestyle='--', lw=2, label='50 cases/100.000')
  ax.set_ylim(bottom=0)
  ax2.set_ylim(bottom=0)
  leg = ax.legend(loc='upper left')
  leg2 = ax2.legend(loc='upper center')
#  ax.add_artist(leg)
#  ax2.get_legend().remove()
#  ax.add_artist(leg2)
#  leg2.loc = 'upper center'
  leg.get_frame().set_edgecolor(cl)
  leg2.get_frame().set_edgecolor(cl2)
  ax.tick_params(axis='x', labelrotation=70)
  ax.xaxis.set_major_formatter(mdates.DateFormatter('%d-%m-%Y'))
  # ax.xaxis.set_major_locator(mdates.DayLocator([interval=7]))
  ax.xaxis.set_major_locator(mdates.WeekdayLocator(byweekday=mpl.dates.MO))
  ax.xaxis.grid(True, which='major')
  for label in ax.get_xticklabels():
      label.set_rotation(40)
      label.set_horizontalalignment('right')
  fig.tight_layout()
  fname = 'plot_{:}.png'.format(k)
  plt.savefig(fname)
#   plt.show()
  return fname, fig, ax, ax2
  

def plot_key_2p(casesMAGS, k):
  fig, axs = plt.subplots(nrows=2, ncols=1, sharex=True, sharey=False, num=None, figsize=(1000/80.,2*400/80. ), dpi=80, facecolor='w', edgecolor='k')
  cl = 'tab:blue'
  cl2 = 'black'
  ax, ax2 = axs
  ax.set_title(ks_names[k])
  ax.set_ylabel("... Cases per 100.000", color=cl)
#  ax2 = ax.twinx()
  ax.tick_params(axis='y', labelcolor=cl)
  ax2.tick_params(axis='y', labelcolor=cl2)
  ax2.set_ylabel("Active Cases per 100.000", color=cl2)
  ax2.plot(( ser_delayed(casesMAGS.loc[k]['cases'], COVIDDELAY_d)/casesMAGS.loc[k]['popul']*100000 ), lw=0.8, ms=1, marker='o', label=' cases - cases-{:}d-earlier'.format(COVIDDELAY_d))  
  ax.plot(( casesMAGS.loc[k]['cases']/casesMAGS.loc[k]['popul']*100000 ),lw=0.99, label='Total Diagnosed')
  ax.plot(( casesMAGS.loc[k]['deaths']*100/casesMAGS.loc[k]['popul']*100000 ),lw=0.99, label='Deaths*100')
  ax.plot(( (casesMAGS.loc[k]['deaths']+casesMAGS.loc[k]['recov'])/casesMAGS.loc[k]['popul']*100000 ),lw=0.99, label='Recoveries+Deaths')
  ax2.plot(( (casesMAGS.loc[k]['cases']-casesMAGS.loc[k]['deaths']-casesMAGS.loc[k]['recov'])/casesMAGS.loc[k]['popul']*100000 ), label='Active')
  ax2.axhline(y=50, color='red', linestyle='--', lw=2, label='50 cases/100.000')
  ax2.plot(( ser_delayed(casesMAGS.loc[k]['cases'], 7)/casesMAGS.loc[k]['popul']*100000 ), lw=0.8, ms=1, marker='o', label=' cases - cases-{:}d-earlier'.format(7))  
  ax2.plot(( ser_delayed(casesMAGS.loc[k]['deaths']*10, 14)/casesMAGS.loc[k]['popul']*100000 ), lw=0.8, ms=1, marker='o', label=' (deaths - deaths-{:}d-earlier)*10'.format(14))  
  ax.set_ylim(bottom=0)
  ax2.set_ylim(bottom=0)
  ax.yaxis.set_ticks_position('right')
  ax2.yaxis.set_ticks_position('right')
  ax.yaxis.set_label_position('right')
  ax2.yaxis.set_label_position('right')
  leg = ax.legend(loc='upper left')
  leg2 = ax2.legend(loc='upper left')
#  ax.add_artist(leg)
#  ax2.get_legend().remove()
#  ax.add_artist(leg2)
#  leg2.loc = 'upper center'
  leg.get_frame().set_edgecolor(cl)
  leg2.get_frame().set_edgecolor(cl2)
#  ax2.tick_params(axis='x', labelrotation=70)
  ax2.xaxis.set_major_formatter(mdates.DateFormatter('%d-%m-%Y'))
  ax.xaxis.set_major_locator(mdates.WeekdayLocator(byweekday=mpl.dates.MO))
  ax.xaxis.grid(True, which='major')
  ax2.xaxis.set_major_locator(mdates.WeekdayLocator(byweekday=mpl.dates.MO))
  ax2.xaxis.grid(True, which='major')
  for label in ax2.get_xticklabels():
      label.set_rotation(40)
      label.set_horizontalalignment('right')
  fig.tight_layout()
  fname = 'plot_{:}.png'.format(k)
  plt.savefig(fname)
#   plt.show()
  return fname, fig, ax, ax2
  
# fn,_,_,_ = plot_key(5334)
  
# plot_key(5334)

def plot_cum(casesMAGS, col, ylabel):
  ti = casesMAGS.loc[5111].index[-1]
  ks_ord = casesMAGS.swaplevel(0,1).loc[pd.IndexSlice[ti]][col].sort_values(ascending=False).index.to_list()

  sp = casesMAGS.loc[5111].shape

  Nc = sp[1]
  cm = plt.get_cmap('gist_ncar')


  fig, ax = plt.subplots(nrows=1, ncols=1, sharex=False, sharey=False, num=None, figsize=(2000/80.,1200/80. ), dpi=80, facecolor='w', edgecolor='k')


  cum = np.zeros(sp[0])
  for i,k in enumerate(ks_ord):
    c = ks_names[k]
    df = casesMAGS.loc[k][col]
    cum += df.values
    ax.fill_between(df.index, cum, cum - df.values, label='MAGS '+c , color=cm(1.*(i%10)/10+0.05))
  ax.set_ylabel(ylabel)
  # ax.set_yscale("log")
  handles, labels = ax.get_legend_handles_labels()
  #ax.legend(handles[::-1], labels[::-1], loc='center left', bbox_to_anchor=(1, 0.5))
  ax.legend(handles[::-1], labels[::-1], loc='upper left', ncol=3,handleheight=2.4, labelspacing=0.05)
  ax.tick_params(axis='x', labelrotation=70)

  # ax.annotate('iljabek@gmail.com', xy = [0.005,0.005], xycoords='axes fraction', size=15, color="#FCFCFC")

  ax.xaxis.set_major_formatter(mdates.DateFormatter('%d-%m-%Y'))
  # ax.xaxis.set_major_locator(mdates.DayLocator([interval=7]))
  ax.xaxis.set_major_locator(mdates.WeekdayLocator(byweekday=mpl.dates.MO))
  ax.xaxis.grid(True, which='major')
  for label in ax.get_xticklabels():
      label.set_rotation(40)
      label.set_horizontalalignment('right')
  fig.tight_layout()
  fname = 'plot_cum_{:}.png'.format(col)
  fig.savefig(fname)
#   plt.show()
  return fname, fig, ax

# fn,_,_ = plot_cum


